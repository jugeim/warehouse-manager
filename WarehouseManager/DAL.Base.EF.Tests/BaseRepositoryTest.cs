using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;
using Contracts.Domain.Base;
using DAL.Base.EF.Tests.TestItems;
using DAL.Base.EF.Tests.TestItems.TestEntities;
using DAL.Base.EF.Tests.TestItems.TestMappers;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Abstractions;

namespace DAL.Base.EF.Tests
{
    public class BaseRepositoryTest : IDisposable
    {

        private readonly TestDbContext _context;

        private readonly BaseRepository<TestDtoEntity, TestDomainEntity, TestDbContext> _entityRepo;
        private readonly BaseRepository<TestDtoUserEntity, TestDomainUserEntity, TestDbContext> _userEntityRepo;

        public BaseRepositoryTest()
        {
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseInMemoryDatabase(databaseName: "BaseRepositoryTestData")
                .Options;

            _context = new TestDbContext(options);

            _entityRepo = 
                new BaseRepository<TestDtoEntity, TestDomainEntity, TestDbContext>(_context, new EntityMapper());
            _userEntityRepo =
                new BaseRepository<TestDtoUserEntity, TestDomainUserEntity, TestDbContext>(_context, new UserEntityMapper());
        }
        
        [Fact]
        public async Task test_FindAsync_givenNotExistingId_thenNull()
        {
            var entity = await _entityRepo.FindAsync(new Guid());

            entity.Should().BeNull();
        }
        
        [Fact]
        public async Task test_FindAsync_entityWithoutUserId_givenExistingId_thenFoundEntity()
        {
            var testEntity = await AddTestEntityToDb(_context);
            
            var entity = await _entityRepo.FindAsync(testEntity.Id);

            entity.Should().NotBeNull();
            entity!.Id.Should().Be(testEntity.Id);
            entity!.Field1.Should().Be((int) testEntity.Field1);
            entity!.Field2.Should().Be(testEntity.Field2);
            entity!.Field3.Should().Be(testEntity.Field3.ToString());

        }
        
        [Fact]
        public async Task test_FindAsync_entityWithUserId_givenValidUserId_thenFoundEntity()
        {
            var testUserEntity = await AddTestUserEntityToDb(_context);

            var entity = await _userEntityRepo.FindAsync(testUserEntity.Id, testUserEntity.AppUserId);

            entity.Should().NotBeNull();
            entity!.Id.Should().Be(testUserEntity.Id);
            entity!.AppUserId.Should().Be(testUserEntity.AppUserId);
            entity!.TestDomainEntityId.Should().Be(testUserEntity.TestDomainEntityId);
            entity!.TestDomainEntityField1.Should().Be(testUserEntity.TestDomainEntity.Field1);
        }
        
        [Fact]
        public async Task test_FindAsync_entityWithUserId_givenInvalidUserId_thenNull()
        {
            var testUserEntity = await AddTestUserEntityToDb(_context);
            var entity = await _userEntityRepo.FindAsync(testUserEntity.Id, Guid.NewGuid());

            entity.Should().BeNull();

        }
        
        [Fact]
        public async Task test_FirstOrDefaultAsync_givenNotExistingId_thenNull()
        {
            var entity = await _entityRepo.FirstOrDefaultAsync(new Guid());

            entity.Should().BeNull();
        }
                
        [Fact]
        public async Task test_FirstOrDefaultAsync_entityWithoutUserId_givenExistingId_thenEntity()
        {
            var testEntity = await AddTestEntityToDb(_context);
            
            var entity = await _entityRepo.FirstOrDefaultAsync(testEntity.Id, noTracking: false);

            entity.Should().NotBeNull();
            entity!.Id.Should().Be(testEntity.Id);
            entity!.Field1.Should().Be((int) testEntity.Field1);
            entity!.Field2.Should().Be(testEntity.Field2);
            entity!.Field3.Should().Be(testEntity.Field3.ToString());
        }

        [Fact]
        public async Task test_FirstOrDefaultAsync_entityWithUserId_givenValidUserId_thenEntity()
        {
            var testUserEntity = await AddTestUserEntityToDb(_context);
            
            var entity = await _userEntityRepo.FirstOrDefaultAsync(testUserEntity.Id, testUserEntity.AppUserId, false);

            entity.Should().NotBeNull();
            entity!.Id.Should().Be(testUserEntity.Id);
            entity!.AppUserId.Should().Be(testUserEntity.AppUserId);
            entity!.TestDomainEntityId.Should().Be(testUserEntity.TestDomainEntityId);
            entity!.TestDomainEntityField1.Should().Be(testUserEntity.TestDomainEntity.Field1);
        }

        
        [Fact]
        public async Task test_FirstOrDefaultAsync_entityWithUserId_givenInvalidUserId_thenNull()
        {
            var testEntity = await AddTestUserEntityToDb(_context);
            var entity = await _entityRepo.FirstOrDefaultAsync(testEntity.Id, Guid.NewGuid());

            entity.Should().BeNull();
        }

        [Fact]
        public async Task test_GetAllAsync_entityWithoutUserId_thenAllEntities()
        {
            var domEntities = await AddTestEntitiesToDb(_context);
            var mapper = new EntityMapper();

            var all = await _entityRepo.GetAllAsync();

            all = all.ToList();
            all.Should().HaveCount(3);
            all.Select(e => e.Field1).Should()
                .Contain(domEntities.Select(e => (int) e.Field1));
            all.Select(e => e.Field2).Should()
                .Contain(domEntities.Select(e => e.Field2));
        }
        
        [Fact]
        public async Task test_GetAllAsync_entityWithUserId_givenValidUserId_thenAllUserEntities()
        {
            var domUserEntities = await AddTestUserEntitiesToDb(_context);
            var userId = domUserEntities[0].AppUserId;

            _context.UserEntities.Count().Should().Be(5);
            // to eagerly load referenced TestEntities set noTracking to false
            var all = await _userEntityRepo.GetAllAsync(userId, false);
            all = all.ToList();
            all.Should().HaveCount(3);
            all.Select(e => e.AppUserId).Should()
                .OnlyContain(e => e.Equals(userId), $"Should return only entities of provided user (with id {userId})");
            all.Select(e => e.TestDomainEntityField1).Should()
                .Contain(domUserEntities.Select(e => e.TestDomainEntity.Field1));
        }
        
        [Fact]
        public async Task test_GetAllAsync_entityWithUserId_givenInvalidUserId_thenEmptyCollection()
        {
            var domUserEntities = await AddTestUserEntitiesToDb(_context);
            var userId = domUserEntities[0].AppUserId;

            _context.UserEntities.Count().Should().Be(5);
            
            var all = await _userEntityRepo.GetAllAsync(Guid.NewGuid());

            all = all.ToList();
            all.Should().HaveCount(0);
        }

        [Fact]
        public async Task test_Add_shouldAddEntity()
        {
            var entityToAdd = new TestDtoEntity {Field1 = 3, Field2 = "f2", Field3 = "val1"};

            var addedEntity = _entityRepo.Add(entityToAdd);

            addedEntity.Should().NotBeNull();
            addedEntity.Id.Should().NotBeEmpty();
            addedEntity.Field1.Should().Be(entityToAdd.Field1);
            addedEntity.Field2.Should().Be(entityToAdd.Field2);
            addedEntity.Field3.Should().Be(DomainEnum.Value1.ToString());

            await _context.SaveChangesAsync();
            var inDb = await _context.Entities.FindAsync(addedEntity.Id);
            inDb.Should().NotBeNull();
        }
        
        [Fact]
        public async Task test_Update_shouldUpdateEntity()
        {
            var entityInDb = await AddTestEntityToDb(_context);
            var entityToUpdate = new TestDtoEntity()
            {
                Id = entityInDb.Id,
                Field1 = 90,
                Field2 = "new str",
                Field3 = "val1"
            };
            
            var updatedEntity = _entityRepo.Update(entityToUpdate);

            updatedEntity.Should().NotBeNull();
            updatedEntity.Id.Should().Be(entityInDb.Id);
            updatedEntity.Field1.Should().Be(entityToUpdate.Field1);
            updatedEntity.Field2.Should().Be(entityToUpdate.Field2);
            updatedEntity.Field3.Should().Be(DomainEnum.Value1.ToString());
        }
        
        [Fact]
        public async Task test_RemoveAsync_entityWithoutUserId_givenExistingId_shouldRemoveEntity()
        {
            var entityInDb = await AddTestEntityToDb(_context);

            var removedEntity = await _entityRepo.RemoveAsync(entityInDb.Id);

            await _context.SaveChangesAsync();

            removedEntity.Should().NotBeNull();
            (await _context.Entities.FindAsync(removedEntity?.Id)).Should().BeNull();
        }
        
        [Fact]
        public async Task test_RemoveAsync_entityWithoutUserId_givenNotExistingId_shouldThrow()
        {
            var invalidId = Guid.NewGuid();

            FluentActions.Awaiting(async () => await _entityRepo.RemoveAsync(invalidId))
                .Should().Throw<NullReferenceException>()
                .Where(e => e.Message.Contains(invalidId.ToString()));
        }
        
        [Fact]
        public async Task test_RemoveAsync_entityWithUserId_givenValidUserId_shouldRemoveEntity()
        {
            var entityInDb = await AddTestUserEntityToDb(_context);

            var removedEntity = await _userEntityRepo.RemoveAsync(entityInDb.Id, entityInDb.AppUserId);

            await _context.SaveChangesAsync();

            removedEntity.Should().NotBeNull();
            (await _context.Entities.FindAsync(removedEntity?.Id)).Should().BeNull();
        }
        
        [Fact]
        public async Task test_RemoveAsync_entityWithUserId_givenInvalidUserId_shouldThrow()
        {
            var entityInDb = await AddTestUserEntityToDb(_context);
            var invalidUserId = Guid.NewGuid();

            FluentActions.Awaiting(async () => await _entityRepo.RemoveAsync(entityInDb.Id, invalidUserId))
                .Should().Throw<NullReferenceException>()
                .Where(e => e.Message.Contains(entityInDb.Id.ToString()));
        }
        
        
        [Fact]
        public async Task test_ExistsAsync_entityWithoutUserId_returnsTrueForExistingEntity()
        {
            var entityInDb = await AddTestEntityToDb(_context);

            (await _entityRepo.ExistsAsync(entityInDb.Id)).Should().BeTrue();
        }

        [Fact]
        public async Task test_ExistsAsync_entityWithoutUserId_returnsFalseForNotExistingEntity()
        {
            (await _entityRepo.ExistsAsync(new Guid())).Should().BeFalse();
        }
        
        [Fact]
        public async Task test_ExistsAsync_entityWithoutUserId_givenUserId_shouldThrow()
        {
            var entityInDb = await AddTestEntityToDb(_context);

            FluentActions.Awaiting(async () => await _entityRepo.ExistsAsync(entityInDb.Id, Guid.NewGuid()))
                .Should().Throw<AuthenticationException>();
        }
        
        [Fact]
        public async Task test_ExistsAsync_entityWithUserId_givenValidUserId_returnsTrue()
        {
            var entityInDb = await AddTestUserEntityToDb(_context);

            (await _userEntityRepo.ExistsAsync(entityInDb.Id, entityInDb.AppUserId)).Should().BeTrue();
        }
        
        [Fact]
        public async Task test_ExistsAsync_entityWithUserId_givenInvalidUserId_returnsFalse()
        {
            var entityInDb = await AddTestUserEntityToDb(_context);
            
            (await _userEntityRepo.ExistsAsync(entityInDb.Id, new Guid())).Should().BeFalse();
        }

        private async Task<TestDomainEntity> AddTestEntityToDb(TestDbContext context)
        {
            await ClearTableData(context, context.Entities);
            TestDomainEntity testEntity = new()
            {
                Id = Guid.NewGuid(),
                Field1 = 4.2,
                Field2 = "Text Field",
                Field3 = DomainEnum.Value2
            };
            
            context.Entities.Add(testEntity);
            
            await context.SaveChangesAsync();

            return testEntity;
        }
        
        private async Task<TestDomainUserEntity> AddTestUserEntityToDb(TestDbContext context)
        {
            await ClearTableData(context, context.UserEntities);
            await ClearTableData(context, context.Users);
            var domEntity = await AddTestEntityToDb(context);
            TestDomainAppUser user = new()
            {
                Id = Guid.NewGuid(),
                Name = "Name",
                Email = "Email"

            };
            TestDomainUserEntity testUserEntity = new()
            {
                Id = Guid.NewGuid(),
                AppUserId = user.Id,
                TestDomainEntity = domEntity,
                TestDomainEntityId = domEntity.Id
            };
            
            context.UserEntities.Add(testUserEntity);
            await context.SaveChangesAsync();

            return testUserEntity;
        }
        
        private async Task<List<TestDomainEntity>> AddTestEntitiesToDb(TestDbContext context)
        {
            await ClearTableData(context, context.Entities);
            var e1 = new TestDomainEntity {Field1 = 1, Field2 = "1"};
            var e2 = new TestDomainEntity {Field1 = 2, Field2 = "2"};
            var e3 = new TestDomainEntity {Field1 = 3, Field2 = "3"};
            context.Entities.AddRange(e1, e2, e3);
            await context.SaveChangesAsync();

            return new List<TestDomainEntity> {e1, e2, e3};
        }
        
        private async Task<List<TestDomainUserEntity>> AddTestUserEntitiesToDb(TestDbContext context)
        {
            await ClearTableData(context, context.UserEntities);
            List<TestDomainEntity> domEntities = await AddTestEntitiesToDb(context);
            TestDomainAppUser user = new() { Id = Guid.NewGuid(), Name = "Name", Email = "Email" };
            TestDomainAppUser other = new() {Id = Guid.NewGuid()};
            
            var e1 = new TestDomainUserEntity { AppUserId = user.Id, TestDomainEntity = domEntities[0]};
            var e2 = new TestDomainUserEntity { AppUserId = user.Id, TestDomainEntity = domEntities[1]};
            var e3 = new TestDomainUserEntity { AppUserId = user.Id, TestDomainEntity = domEntities[2]};
            
            var other1 = new TestDomainUserEntity { AppUserId = other.Id, TestDomainEntity = domEntities[1]};
            var other2 = new TestDomainUserEntity { AppUserId = other.Id, TestDomainEntity = domEntities[2]};
            context.UserEntities.AddRange(e1, e2, e3, other1, other2);
            await context.SaveChangesAsync();

            return new List<TestDomainUserEntity> {e1, e2, e3, other1, other2};
        }

        private async Task ClearTableData<TEntity>(TestDbContext context, DbSet<TEntity> set)
            where TEntity: class, IDomainEntityId, new()
        {
            foreach (var id in set.Select(e => e.Id))
            {
                var entity = new TEntity { Id = id };
                set.Attach(entity);
                set.Remove(entity);
            }
            await context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}