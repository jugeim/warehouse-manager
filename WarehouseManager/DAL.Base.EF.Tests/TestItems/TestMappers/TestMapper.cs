using Contracts.DAL.Base;

namespace DAL.Base.EF.Tests.TestItems.TestMappers
{
    public abstract class TestMapper<TLeftEntity, TRightEntity> : IBaseMapper<TLeftEntity, TRightEntity>
    {
        public abstract TLeftEntity? Map(TRightEntity? inObject);
        
        public abstract TRightEntity? Map(TLeftEntity? inObject);
    }
}