using DAL.Base.EF.Tests.TestItems.TestEntities;

namespace DAL.Base.EF.Tests.TestItems.TestMappers
{
    public class EntityMapper : TestMapper<TestDtoEntity, TestDomainEntity>
    {
        public override TestDomainEntity? Map(TestDtoEntity? inObject)
        {
            if (inObject == null) return null;

            var field3Value = inObject.Field3.EndsWith('1') ? DomainEnum.Value1
                : inObject.Field3.EndsWith('2') ? DomainEnum.Value2 : DomainEnum.Value3;

            return new TestDomainEntity
            {
                Id = inObject.Id,
                Field1 = inObject.Field1,
                Field2 = inObject.Field2,
                Field3 = field3Value
            };
        }

        public override TestDtoEntity? Map(TestDomainEntity? inObject)
        {
            if (inObject == null) return null;

            return new TestDtoEntity
            {
                Id = inObject.Id,
                Field1 = (int) inObject.Field1,
                Field2 = inObject.Field2,
                Field3 = inObject.Field3.ToString()
            };
        }
    }
}