using DAL.Base.EF.Tests.TestItems.TestEntities;

namespace DAL.Base.EF.Tests.TestItems.TestMappers
{
    public class UserEntityMapper : TestMapper< TestDtoUserEntity, TestDomainUserEntity>
    {
        public override TestDomainUserEntity? Map(TestDtoUserEntity? inObject)
        {
            if (inObject == null) return null;

            return new TestDomainUserEntity
            {
                Id = inObject.Id,
                AppUserId = inObject.AppUserId,
                TestDomainEntityId = inObject.TestDomainEntityId,
                TestDomainEntity = new TestDomainEntity()
            };
        }

        public override TestDtoUserEntity? Map(TestDomainUserEntity? inObject)
        {
            if (inObject == null) return null;

            return new TestDtoUserEntity
            {
                Id = inObject.Id,
                AppUserId = inObject.AppUserId,
                TestDomainEntityField1 = inObject.TestDomainEntity?.Field1 ?? double.NaN,
                TestDomainEntityId = inObject.TestDomainEntityId
            };
        }
    }
}