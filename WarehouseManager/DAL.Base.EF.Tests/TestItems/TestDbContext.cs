using System;
using DAL.Base.EF.Tests.TestItems.TestEntities;
using Microsoft.EntityFrameworkCore;

namespace DAL.Base.EF.Tests.TestItems
{
    public class TestDbContext: DbContext
    {
        public DbSet<TestDomainEntity> Entities { get; set; } = default!;
        public DbSet<TestDomainUserEntity> UserEntities { get; set; } = default!;
        public DbSet<TestDomainAppUser> Users { get; set; } = default!;
        
        public TestDbContext(DbContextOptions<TestDbContext> options)
            : base(options)
        {
           
        }
    }
}