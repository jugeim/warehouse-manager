using System;
using Contracts.Domain.Base;

namespace DAL.Base.EF.Tests.TestItems.TestEntities
{
    public class TestDomainEntity : IDomainEntityId
    {
        public Guid Id { get; set; }
        
        public double Field1 { get; set; }

        public string Field2 { get; set; } = default!;
        
        public DomainEnum Field3 { get; set; }

    }

    public enum DomainEnum
    {
        Value1,
        Value2,
        Value3
    }
}