using System;
using Contracts.Domain.Base;

namespace DAL.Base.EF.Tests.TestItems.TestEntities
{
    public class TestDtoUserEntity : IDomainEntityId, IDomainAppUserId
    {
        public Guid Id { get; set; }
        public Guid AppUserId { get; set; }
        
        public Guid TestDomainEntityId { get; set; }
        public double? TestDomainEntityField1 { get; set; }
    }
}