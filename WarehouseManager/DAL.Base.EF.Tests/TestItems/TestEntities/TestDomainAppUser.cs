using System;
using Contracts.Domain.Base;

namespace DAL.Base.EF.Tests.TestItems.TestEntities
{
    public class TestDomainAppUser : IDomainEntityId
    {
        public Guid Id { get; set; }

        public string Name { get; set; } = default!;

        public string Email { get; set; } = default!;
    }
}