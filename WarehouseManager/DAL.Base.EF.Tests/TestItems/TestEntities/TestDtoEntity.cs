using System;
using Contracts.Domain.Base;

namespace DAL.Base.EF.Tests.TestItems.TestEntities
{
    public class TestDtoEntity : IDomainEntityId
    {
        public Guid Id { get; set; } = default!;
        
        public int Field1 { get; set; }

        public string Field2 { get; set; } = default!;

        public string Field3 { get; set; } = default!;
    }
}