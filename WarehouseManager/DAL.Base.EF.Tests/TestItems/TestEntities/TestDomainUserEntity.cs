using System;
using Contracts.Domain.Base;

namespace DAL.Base.EF.Tests.TestItems.TestEntities
{
    public class TestDomainUserEntity : IDomainEntityId, IDomainAppUserId
    {
        public Guid Id { get; set; }
        public Guid AppUserId { get; set; }
        
        public Guid TestDomainEntityId { get; set; } = default!;
        public TestDomainEntity TestDomainEntity { get; set; } = default!;
    }
}