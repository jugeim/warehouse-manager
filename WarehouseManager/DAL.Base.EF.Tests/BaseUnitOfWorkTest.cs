using System;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using DAL.Base.EF.Tests.TestItems;
using DAL.Base.EF.Tests.TestItems.TestEntities;
using DAL.Base.EF.Tests.TestItems.TestMappers;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Xunit;
using Xunit.Abstractions;

namespace DAL.Base.EF.Tests
{
    public class BaseUnitOfWorkTest
    {
        private readonly ITestOutputHelper _testOutputHelper;
        private readonly TestDbContext _context;
        private readonly BaseUnitOfWork<TestDbContext> _testUow;

        public BaseUnitOfWorkTest(ITestOutputHelper testOutputHelper)
        {
            _testOutputHelper = testOutputHelper;
            var options = new DbContextOptionsBuilder<TestDbContext>()
                .UseInMemoryDatabase(databaseName: "BaseRepositoryTestData")
                .Options;

            _context = new TestDbContext(options);
            _testUow = new BaseUnitOfWork<TestDbContext>(_context);
        }

        [Fact]
        public void test_GetRepository_shouldReturnSameRepository()
        {
            var mapper = new EntityMapper();
            var repoCreationMethod = new Func<BaseRepository<TestDtoEntity, TestDomainEntity, TestDbContext>>(
                () => new BaseRepository<TestDtoEntity, TestDomainEntity, TestDbContext>(_context, mapper));

            var uowRepository = _testUow.GetRepository(repoCreationMethod);

            var sameUowRepository = _testUow.GetRepository(repoCreationMethod);
            
            Assert.True(uowRepository == sameUowRepository, 
                "Received from UOW repositories should reference the same object");
        }

        [Fact]
        public async Task test_SaveChangesAsync_savesChanges()
        {
            var mapper = new EntityMapper();
            var repoCreationMethod = new Func<BaseRepository<TestDtoEntity, TestDomainEntity, TestDbContext>>(
                () => new BaseRepository<TestDtoEntity, TestDomainEntity, TestDbContext>(_context, mapper));

            var uowRepository = _testUow.GetRepository(repoCreationMethod);

            uowRepository.Add(new TestDtoEntity() {Field1 = 1, Field2 = "", Field3 = "1"});
            uowRepository.Add(new TestDtoEntity() {Field1 = 2, Field2 = "", Field3 = "2"});
            uowRepository.Add(new TestDtoEntity() {Field1 = 3, Field2 = "", Field3 = "3"});

            _context.Entities.Should().BeEmpty();

            await _testUow.SaveChangesAsync();

            _context.Entities.Should().HaveCount(3);

        }
    }
}