using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Contracts.Domain.Base;

namespace Contracts.DAL.Base
{
    public interface IBaseRepository<TEntity> : IBaseRepository<TEntity, Guid>
        where TEntity : class, IDomainEntityId
    {
        
    }
    
    public interface IBaseRepository<TEntity, TKey> : ICrudRepository<TEntity, TKey>
        where TEntity : class, IDomainEntityId<TKey>
        where TKey : struct, IEquatable<TKey>
    {
        /// <summary>
        /// Method will search for desired Entity in given DbContext, if not found then db query will be executed
        /// Always returns tracked Entity.
        /// </summary>
        /// <param name="id">Entity Primary Key</param>
        /// <param name="userId">User Id shall be provided if entity is user specific</param>
        /// <returns>Task => Found Entity or null if not found</returns>
        Task<TEntity?> FindAsync(TKey id, TKey? userId = default);
        
        /// <summary>
        /// Method will execute the db query immediately.
        /// Should allows not tracked Entity return. 
        /// Most probably should be used instead of <see cref="FindAsync"/> in case <c>noTracing</c>
        /// flag was applied on desired Entity earlier  
        /// </summary>
        /// <param name="id">Entity Primary Key</param>
        /// <param name="userId">User Id shall be provided if entity is user specific</param>
        /// <param name="noTracking">
        /// Set true in case Entity should not be tracked by default. Will increase performance speed.  
        /// </param>
        /// <returns>Task => Found Entity or <c>default</c> (<c>null</c> in most cases) if not found</returns>
        Task<TEntity?> FirstOrDefaultAsync(TKey id, TKey? userId = default, bool noTracking = true);

        Task<IEnumerable<TEntity>> GetAllAsync(TKey? userId = default, bool noTracking = true);
        
        Task<bool> ExistsAsync(TKey id, TKey? userId = default);
        
        Task<TEntity?> RemoveAsync(TKey id, TKey? userId = default);
    }
}