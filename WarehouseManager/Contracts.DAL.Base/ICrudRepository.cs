﻿using System;
using Contracts.Domain.Base;

namespace Contracts.DAL.Base
{
    public interface ICrudRepository<TEntity, TKey>
        where TEntity : class, IDomainEntityId<TKey>
        where TKey : struct, IEquatable<TKey>
    {
        // repository most common methods
        TEntity Add(TEntity entity);
        TEntity Update(TEntity entity);
        TEntity Remove(TEntity entity, TKey? userId = default);
    }
}