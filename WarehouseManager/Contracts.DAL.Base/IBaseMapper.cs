namespace Contracts.DAL.Base
{
    public interface IBaseMapper<TLeftObject, TRightObject>
    {
        TLeftObject? Map(TRightObject? inObject);
        TRightObject? Map(TLeftObject? inObject);
    }
}