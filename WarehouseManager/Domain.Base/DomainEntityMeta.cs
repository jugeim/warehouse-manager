﻿using System;

namespace Domain.Base
{
    public class DomainEntityMeta
    {
        public string CreatedBy { get; set; } = "system";
        public DateTime CreateAt { get; set; }
        public string UpdateBy { get; set; } = "system";
        public DateTime UpdatedAt { get; set; }
    }
}