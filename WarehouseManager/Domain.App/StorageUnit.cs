using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Contracts.Domain.Base;
using Domain.App.Identity;

namespace Domain.App
{
    public class StorageUnit: IDomainEntityId, IDomainAppUserId, IDomainAppUser<AppUser>
    {
        public Guid Id { get; set; }
        
        // storage is always user-specific
        public Guid AppUserId { get; set; }
        public AppUser AppUser { get; set; } = default!;

        // to allow may levels of storing cells
        [ForeignKey(nameof(DirectoryStorage))]
        public Guid? DirectoryStorageId { get; set; }
        public StorageUnit? DirectoryStorage { get; set; }

        public string Name { get; set; } = default!;
        
        public double? MaxVolume { get; set; }

        
        public ICollection<StorageUnit>? StorageCells { get; set; }
        
        public ICollection<ItemInStorageUnit>? StoredItems { get; set; }
        
    }
}