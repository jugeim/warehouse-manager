using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Contracts.Domain.Base;
using Domain.App.Identity;

namespace Domain.App
{
    public class ItemCategory: IDomainEntityId, IDomainOptionalAppUserId, IDomainAppUser<AppUser>
    {
        public Guid Id { get; set; }

        // There are predefined item categories but they can be custom (made by user and belonging only to him)
        public Guid? AppUserId { get; set; } = default!;
        public AppUser? AppUser { get; set; } = default!;
        
        // To allow many levels of subcategories
        [ForeignKey(nameof(CatalogCategory))]
        public Guid? CatalogCategoryId { get; set; }
        public ItemCategory? CatalogCategory { get; set; }

        public string Name { get; set; } = default!;

        public bool ShouldHaveSerialNumber { get; set; } = false;
        public bool ShouldHavePrice { get; set; } = false;

        public ICollection<ItemCategory>?  SubCategories { get; set; }
        public ICollection<Item>?  Items { get; set; }
    }
}