using System;
using Contracts.Domain.Base;

namespace Domain.App
{
    public class ItemInStorageUnit: IDomainEntityId
    {
        public Guid Id { get; set; }
        
        public Guid ItemId { get; set; } = default!;
        public Item Item { get; set; } = default!;
        
        public Guid StorageUnitId { get; set; } = default!;
        public StorageUnit StorageUnit { get; set; } = default!;

        public double Count { get; set; } = 1;
        
    }
}