﻿using System;
using System.Collections.Generic;
using Contracts.Domain.Base;
using Domain.App.Identity;

namespace Domain.App
{
    public class Item: IDomainEntityId, IDomainOptionalAppUserId, IDomainAppUser<AppUser>
    {
        public Guid Id { get; set; }
        
        public Guid? AppUserId { get; set; }
        public AppUser? AppUser { get; set; }

        public string Name { get; set; } = default!;
        
        // save reference to the picture?
        public string? PictureUrl { get; set; }
        
        public string? SerialNumber { get; set; }
        
        public double? Volume { get; set; }
        
        public double? Price { get; set; }

        public Guid ItemCategoryId { get; set; } = default!;
        public ItemCategory ItemCategory { get; set; } = default!;
        
        
        public ICollection<ItemInStorageUnit>? ItemInStorageUnits { get; set; }
    }
}