using System;

namespace Contracts.Domain.Base
{
    public interface IDomainOptionalAppUserId: IDomainOptionalAppUserId<Guid>
    {
        
    }

    public interface IDomainOptionalAppUserId<TKey>
        where TKey: struct, IEquatable<TKey>
    {
        TKey? AppUserId { get; set; }
    }

    public interface IDomainAppUserId : IDomainAppUserId<Guid>
    {
        
    }

    public interface IDomainAppUserId<TKey>
        where TKey : struct, IEquatable<TKey>
    {
        TKey AppUserId { get; set; }
    }
}