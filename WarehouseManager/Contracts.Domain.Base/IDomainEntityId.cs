using System;

namespace Contracts.Domain.Base
{
    public interface IDomainEntityId : IDomainEntityId<Guid>
    {
        
    }
    
    // Contract, that requires equatable property Id  
    public interface IDomainEntityId<TKey>
        where TKey : struct, IEquatable<TKey>
    {
        TKey Id { get; set; }
    } 
}