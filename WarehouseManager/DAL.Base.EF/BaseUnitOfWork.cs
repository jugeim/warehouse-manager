using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Contracts.DAL.Base;
using Microsoft.EntityFrameworkCore;

namespace DAL.Base.EF
{

    public class BaseUnitOfWork<TDbContext> : IBaseUnitOfWork
        where TDbContext : DbContext
    {
        private readonly Dictionary<Type, object> _repositoryCache = new();
        
        protected readonly TDbContext UowDbContext;

        public BaseUnitOfWork(TDbContext uowDbContext)
        {
            UowDbContext = uowDbContext;
        }
        
        public Task<int> SaveChangesAsync()
        {
            return UowDbContext.SaveChangesAsync();
        }
        
        public TRepository GetRepository<TRepository>(Func<TRepository> repositoryCreationMethod)
            where TRepository : class
        {
            if (_repositoryCache.TryGetValue(typeof(TRepository), out var repository))
            {
                return (TRepository) repository;
            }

            var repositoryInstance = repositoryCreationMethod();
            _repositoryCache.Add(typeof(TRepository), repositoryInstance);
            return repositoryInstance;
        }
        
    }
}