﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Authentication;
using System.Threading.Tasks;
using Contracts.DAL.Base;
using Contracts.Domain.Base;
using Microsoft.EntityFrameworkCore;

namespace DAL.Base.EF
{
    public class BaseRepository<TDtoEntity, TDomainEntity, TDbContext> : 
        BaseRepository<TDtoEntity, TDomainEntity, Guid, TDbContext>, 
        IBaseRepository<TDtoEntity>
        where TDtoEntity : class, IDomainEntityId
        where TDomainEntity : class, IDomainEntityId
        where TDbContext : DbContext
    {
        public BaseRepository(TDbContext dbContext, IBaseMapper<TDtoEntity, TDomainEntity> mapper) 
            : base(dbContext, mapper)
        {
            
        }
        
    }
    
    public class BaseRepository<TDtoEntity, TDomainEntity, TKey, TDbContext> : 
        IBaseRepository<TDtoEntity, TKey>
        where TDtoEntity : class, IDomainEntityId<TKey>
        where TDomainEntity : class, IDomainEntityId<TKey>
        where TKey : struct, IEquatable<TKey>
        where TDbContext : DbContext
    {
        protected enum DomainEntityType
        {
            NoUser, OptUser, WithUser
        }
        
        protected readonly TDbContext RepoDbContext;
        protected readonly DbSet<TDomainEntity> RepoDbSet;
        protected readonly IBaseMapper<TDtoEntity, TDomainEntity> Mapper;

        private DomainEntityType _repoDomainEntityType;

        public BaseRepository(TDbContext dbContext, IBaseMapper<TDtoEntity, TDomainEntity> mapper)
        {
            RepoDbContext = dbContext;
            RepoDbSet = dbContext.Set<TDomainEntity>();
            Mapper = mapper;
            _repoDomainEntityType = GetRepositoryDomainEntityType();
        }
        
        protected IQueryable<TDomainEntity> CreateQuery(TKey? userId = default, bool noTracking = true)
        {
            var query = RepoDbSet.AsQueryable();

            if (userId != null && !userId.Equals(default))
            {
                // ReSharper disable once SuspiciousTypeConversion.Global
                switch (_repoDomainEntityType)
                {
                    case DomainEntityType.WithUser:
                        query = query.Where(e => userId.Equals((e as IDomainAppUserId<TKey>)!.AppUserId));
                        break;
                    case DomainEntityType.OptUser:
                        query = query.Where(e => userId.Equals((e as IDomainOptionalAppUserId<TKey>)!.AppUserId));
                        break;
                    case DomainEntityType.NoUser:
                        break;
                }
            }

            if (noTracking) query = query.AsNoTracking();

            return query;
        }

        /// <summary>
        /// Repository is for AppUserEntity if DomainEntity has the AppUser(Id) property (can be optional)
        /// </summary>
        /// <returns>true if domain entity has AppUserId property</returns>
        protected virtual bool IsAppUserEntityRepository()
        {
            return _repoDomainEntityType == DomainEntityType.OptUser ||
                   _repoDomainEntityType == DomainEntityType.WithUser;
        }
        
        protected DomainEntityType GetRepositoryDomainEntityType()
        {
            if (typeof(TDomainEntity).IsAssignableTo(typeof(IDomainAppUserId)))
                return DomainEntityType.WithUser;
            if (typeof(TDomainEntity).IsAssignableTo(typeof(IDomainOptionalAppUserId)))
                return DomainEntityType.OptUser;
            return DomainEntityType.NoUser;
        }

        public virtual async Task<IEnumerable<TDtoEntity>> GetAllAsync(TKey? userId = default, bool noTracking = true)
        {
            var query = CreateQuery(userId, noTracking);
            var data = await query.ToListAsync();
            var res = data.Select(domainEntity => Mapper.Map(domainEntity));

            return res!;
        }

        public virtual async Task<TDtoEntity?> FindAsync(TKey id, TKey? userId = default)
        {
            var res = await RepoDbSet.FindAsync(id);

            if (userId != null && !userId.Equals(default) && IsAppUserEntityRepository())
            {   // if found item does not belong to provided user, then ->
                if (!userId.Equals(GetEntityAppUserId(res))) return null;
            }

            return Mapper.Map(res);
        }

        public virtual async Task<TDtoEntity?> FirstOrDefaultAsync(TKey id, TKey? userId = default,
            bool noTracking = true)
        {
            var query = CreateQuery(userId, noTracking);

            return Mapper.Map(await query.FirstOrDefaultAsync(e => e.Id.Equals(id)));
        }

        public virtual TDtoEntity Add(TDtoEntity entity)
        {
            return Mapper.Map(RepoDbSet.Add(Mapper.Map(entity)!).Entity)!;
        }

        public virtual TDtoEntity Update(TDtoEntity entity)
        {
            var entityToUpdate = Mapper.Map(entity)!;
            var inDb = RepoDbSet.Find(entityToUpdate.Id);
            if (inDb != null) RepoDbContext.Entry(inDb).State = EntityState.Detached;
            var updated= RepoDbSet.Update(entityToUpdate);
            
            return Mapper.Map(updated.Entity)!;
        }

        // RemoveAsync shall be used to remove entities instead 
        public virtual TDtoEntity Remove(TDtoEntity entity, TKey? userId = default)
        {
            if (userId != null && !userId.Equals(default) &&
                typeof(IDomainAppUserId<TKey>).IsAssignableFrom(typeof(TDomainEntity)) &&
                !((IDomainAppUserId<TKey>) entity).AppUserId.Equals(userId))
            {
                throw new AuthenticationException(
                    $"Bad user id inside entity {typeof(TDtoEntity).Name} to be deleted.");
            }
            var inDb = RepoDbSet.Find(entity.Id);
            if (inDb == null) return entity;
            return Mapper.Map(RepoDbSet.Remove(inDb).Entity)!;
        }

        public virtual async Task<TDtoEntity?> RemoveAsync(TKey id, TKey? userId = default)
        {
            var entity = await FirstOrDefaultAsync(id, userId);
            if (entity == null)
            {
                throw new NullReferenceException($"Entity {typeof(TDtoEntity).Name} with id {id} not found.");
            }
            return Remove(entity!, userId);
        }

        public virtual async Task<bool> ExistsAsync(TKey id, TKey? userId = default)
        {
            if (userId == null || userId.Equals(default))
            {
                // no ownership control, userId was null or default
                return await RepoDbSet.AnyAsync(e => e.Id.Equals(id));
            }
            
            if (!IsAppUserEntityRepository())
            {
                throw new AuthenticationException(
                    $"Entity {typeof(TDomainEntity).Name} does not implement required interface: " +
                    $"{typeof(IDomainAppUserId<TKey>).Name} for AppUserId check");
            }

            return await CreateQuery(userId).AnyAsync();

        }
        
        private TKey? GetEntityAppUserId(TDomainEntity entity)
        {
            if (typeof(IDomainAppUserId<TKey>).IsAssignableFrom(typeof(TDomainEntity)))
            {
                return ((IDomainAppUserId<TKey>) entity).AppUserId;
            }
            if (typeof(IDomainOptionalAppUserId<TKey>).IsAssignableFrom(typeof(TDomainEntity)))
            {
                return ((IDomainOptionalAppUserId<TKey>) entity).AppUserId;
            }
            return null;
        }

    }
}