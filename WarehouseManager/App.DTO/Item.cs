using System;
using Contracts.Domain.Base;
using Domain.App;

namespace App.DTO
{
    public class Item : IDomainEntityId, IDomainOptionalAppUserId
    {
        public Guid Id { get; set; }
        
        public Guid? AppUserId { get; set; }

        public string Name { get; set; } = default!;
        
        // save reference to the picture?
        public string? PictureUrl { get; set; }
        
        public string? SerialNumber { get; set; }
        
        public double? Volume { get; set; }
        
        public double? Price { get; set; }

        public ItemCategory ItemCategory { get; set; } = default!;
    }
}