using AutoMapper;

namespace App.DTO.MappingProfiles
{
    public class AutoMapperProfile: Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<App.DTO.Item, Domain.App.Item>().ReverseMap();
            CreateMap<App.DTO.ItemCateogry, Domain.App.ItemCategory>().ReverseMap();
            CreateMap<App.DTO.StorageUnit, Domain.App.StorageUnit>().ReverseMap();
            CreateMap<App.DTO.ItemInStorageUnit, Domain.App.ItemInStorageUnit>().ReverseMap();
            CreateMap<App.DTO.Identity.AppUser, Domain.App.Identity.AppUser>().ReverseMap();
            CreateMap<App.DTO.Identity.AppRole, Domain.App.Identity.AppRole>().ReverseMap();
        }
    }
}