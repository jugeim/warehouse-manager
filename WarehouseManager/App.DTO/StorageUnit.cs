using System;
using System.ComponentModel.DataAnnotations.Schema;
using Contracts.Domain.Base;

namespace App.DTO
{
    public class StorageUnit : IDomainEntityId, IDomainAppUserId
    {
        public Guid Id { get; set; }
        
        // storage is always user-specific
        public Guid AppUserId { get; set; }
        
        public Guid? DirectoryStorageId { get; set; }

        public string Name { get; set; } = default!;
        
        public double? MaxVolume { get; set; }
    }
}