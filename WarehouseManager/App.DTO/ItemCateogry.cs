using System;
using System.ComponentModel.DataAnnotations.Schema;
using Contracts.Domain.Base;
using Domain.App;

namespace App.DTO
{
    public class ItemCateogry : IDomainEntityId, IDomainOptionalAppUserId
    {
        public Guid Id { get; set; }

        // There are predefined item categories but they can be custom (made by user and belonging only to him)
        public Guid? AppUserId { get; set; } = default!;
        
        public Guid? CatalogCategoryId { get; set; }

        public string Name { get; set; } = default!;

        public bool ShouldHaveSerialNumber { get; set; } = false;
        public bool ShouldHavePrice { get; set; } = false;
    }
}