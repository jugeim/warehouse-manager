using System;
using System.ComponentModel.DataAnnotations;
using Contracts.Domain.Base;

namespace App.DTO.Identity
{
    public class AppUser : IDomainEntityId
    {
        public Guid Id { get; set; }
        
        [StringLength(128, MinimumLength = 1)]
        public string Firstname { get; set; } = default!;
        
        [StringLength(128, MinimumLength = 1)]
        public string? Lastname { get; set; } = default!;
    }
}