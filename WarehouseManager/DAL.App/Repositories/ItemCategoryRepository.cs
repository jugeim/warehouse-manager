using AutoMapper;
using Contracts.DAL.App.Repositories;
using DAL.App.Mappers;
using DAL.Base.EF;
using DtoItemCategory = App.DTO.ItemCateogry;
using DomainItemCategory = Domain.App.ItemCategory;

namespace DAL.App.Repositories
{
    public class ItemCategoryRepository : BaseRepository<DtoItemCategory, DomainItemCategory, AppDbContext>, IItemCategoryRepository
    {
        public ItemCategoryRepository(AppDbContext dbContext, IMapper mapper) : 
            base(dbContext, new BaseMapper<DtoItemCategory, DomainItemCategory>(mapper))
        {
        }
    }
}