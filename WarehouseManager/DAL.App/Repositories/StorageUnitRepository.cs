using AutoMapper;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.App.Mappers;
using DAL.Base.EF;
using DtoStorageUnit = App.DTO.StorageUnit;
using DomainStorageUnit = Domain.App.StorageUnit;

namespace DAL.App.Repositories
{
    public class StorageUnitRepository : BaseRepository<DtoStorageUnit, DomainStorageUnit, AppDbContext>, IStorageUnitRepository

    {
        public StorageUnitRepository(AppDbContext dbContext, IMapper mapper) : 
            base(dbContext, new BaseMapper<DtoStorageUnit, DomainStorageUnit>(mapper))
        {
        }
    }
}