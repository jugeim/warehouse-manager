using AutoMapper;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.App.Mappers;
using DAL.Base.EF;
using DtoItem = App.DTO.Item;
using DomainItem = Domain.App.Item;

namespace DAL.App.Repositories
{
    public class ItemRepository : BaseRepository<DtoItem, DomainItem, AppDbContext>, IItemRepository
    {
        public ItemRepository(AppDbContext dbContext, IMapper mapper) : 
            base(dbContext, new BaseMapper<DtoItem, DomainItem>(mapper))
        {
        }
    }
}