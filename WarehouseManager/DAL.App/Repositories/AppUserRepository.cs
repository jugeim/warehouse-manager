using AutoMapper;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.App.Mappers;
using DAL.Base.EF;
using DtoAppUser = App.DTO.Identity.AppUser;
using DomainAppUser = Domain.App.Identity.AppUser;

namespace DAL.App.Repositories
{
    public class AppUserRepository : BaseRepository<DtoAppUser, DomainAppUser, AppDbContext>, IAppUserRepository
    {
        public AppUserRepository(AppDbContext dbContext, IMapper mapper) 
            : base(dbContext, new BaseMapper<DtoAppUser, DomainAppUser>(mapper))
        {
        }
    }
}