﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using Contracts.DAL.App;
using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;
using DAL.App.Repositories;
using DAL.Base.EF;

namespace DAL.App
{
    public class AppUnitOfWork : BaseUnitOfWork<AppDbContext>, IAppUnitOfWork
    {
        protected IMapper Mapper;
        
        public AppUnitOfWork(AppDbContext uowDbContext, IMapper mapper) : base(uowDbContext)
        {
            Mapper = mapper;
        }

        public IAppUserRepository Users => GetRepository(() => new AppUserRepository(UowDbContext, Mapper));
        
        public IItemCategoryRepository ItemCategories => GetRepository(() => new ItemCategoryRepository(UowDbContext, Mapper));
        
        public IItemRepository Items => GetRepository(() => new ItemRepository(UowDbContext, Mapper));
        
        public IStorageUnitRepository StorageUnits => GetRepository(() => new StorageUnitRepository(UowDbContext, Mapper));
        
    }
}