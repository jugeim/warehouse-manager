﻿using Contracts.DAL.App.Repositories;
using Contracts.DAL.Base;

namespace Contracts.DAL.App
{
    public interface IAppUnitOfWork : IBaseUnitOfWork
    {
        public IAppUserRepository Users { get; }
        public IItemCategoryRepository ItemCategories { get; }
        public IItemRepository Items { get; }
        public IStorageUnitRepository StorageUnits { get; }
    }
}