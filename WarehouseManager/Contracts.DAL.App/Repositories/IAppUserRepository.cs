using Contracts.DAL.Base;
using DtoAppUser = App.DTO.Identity.AppUser;

namespace Contracts.DAL.App.Repositories
{
    public interface IAppUserRepository : IBaseRepository<DtoAppUser>
    {
        
    }
}