using Contracts.DAL.Base;
using DtoStorageUnit = App.DTO.StorageUnit;

namespace Contracts.DAL.App.Repositories
{
    public interface IStorageUnitRepository : IBaseRepository<DtoStorageUnit>
    {
        
    }
}