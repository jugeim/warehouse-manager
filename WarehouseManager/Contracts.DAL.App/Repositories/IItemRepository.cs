using Contracts.DAL.Base;
using DtoItem = App.DTO.Item;

namespace Contracts.DAL.App.Repositories
{
    public interface IItemRepository : IBaseRepository<DtoItem>
    {
        
    }
}