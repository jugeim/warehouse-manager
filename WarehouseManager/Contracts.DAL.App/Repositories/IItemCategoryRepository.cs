using Contracts.DAL.Base;
using DtoItemCategory = App.DTO.ItemCateogry;

namespace Contracts.DAL.App.Repositories
{
    public interface IItemCategoryRepository : IBaseRepository<DtoItemCategory>
    {
        
    }
}